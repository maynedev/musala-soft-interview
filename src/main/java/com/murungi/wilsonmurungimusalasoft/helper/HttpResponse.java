package com.murungi.wilsonmurungimusalasoft.helper;

import lombok.Data;

public @Data
class HttpResponse {
    private String message;
    private Object payload;

    public HttpResponse(String message, Object payload) {
        this.message = message;
        this.payload = payload;
    }
}
