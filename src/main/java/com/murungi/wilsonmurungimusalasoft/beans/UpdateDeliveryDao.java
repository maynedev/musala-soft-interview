package com.murungi.wilsonmurungimusalasoft.beans;

import com.murungi.wilsonmurungimusalasoft.model.customValidation.AnyOf;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class UpdateDeliveryDao {
    @NotNull
    @AnyOf(enumClass = DeliveryState.class, message = "must be of any LOADING, DISPATCHED, DELIVERED")
    private String status;
}

enum DeliveryState {
    LOADING, DISPATCHED, DELIVERED
}