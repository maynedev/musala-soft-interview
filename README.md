# Interview submission for Murungi Wilson

_Database in-memory H2_

_No preloaded data_

_Logging is on the console_

### Pre-script
Make maven script executable. Run

`chmod +x ./mvnw` - On LINUX OS

Windows* 
Use `mvnw.cmd` script
### Run Tests
`./mvnw test`

### Run package
`./mvnw package`

### Run application
`java -jar target/murungiWilsonMusala.jar`

Service will run on port 8080. This can be changed on the application.properties file

Change below line to your preferred port

`server.port=8080`

## **Sample requests Postman Collection**


[https://go.postman.co/workspace/My-Workspace~4c1888b1-0eeb-4dc4-8bee-7e9523f69814/collection/2191480-c5115dcd-c6f2-47fc-b7ea-4c87c6a78dfb][Postman collection]

[Postman collection]: https://go.postman.co/workspace/My-Workspace~4c1888b1-0eeb-4dc4-8bee-7e9523f69814/collection/2191480-c5115dcd-c6f2-47fc-b7ea-4c87c6a78dfb