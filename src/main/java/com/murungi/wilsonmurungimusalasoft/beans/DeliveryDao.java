package com.murungi.wilsonmurungimusalasoft.beans;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

public @Data
class DeliveryDao {
    @NotNull
    private long droneId;
    @NotNull
    private List<DeliveryMedicationDao> medications;
}


