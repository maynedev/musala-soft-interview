package com.murungi.wilsonmurungimusalasoft.model;

import lombok.Data;

import javax.persistence.*;

@Entity
public @Data
class DeliveryMedication {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private int units;

    @OneToOne
    @JoinColumn(name = "medication_id")
    private Medication medication;

    @ManyToOne
    @JoinColumn(name = "delivery_id")
    private Delivery delivery;

    public DeliveryMedication( Medication medication, int units, Delivery delivery) {
        this.units = units;
        this.medication = medication;
        this.delivery = delivery;
    }

    public DeliveryMedication() {

    }
}
