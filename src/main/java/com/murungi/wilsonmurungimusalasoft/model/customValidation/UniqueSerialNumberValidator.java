package com.murungi.wilsonmurungimusalasoft.model.customValidation;

import com.murungi.wilsonmurungimusalasoft.repository.DroneRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UniqueSerialNumberValidator implements ConstraintValidator<UniqueSerialNumber, String> {
    @Autowired
    DroneRepository droneRepository;

    @Override
    public boolean isValid(String serialNumber, ConstraintValidatorContext constraintValidatorContext) {
        return droneRepository == null || !droneRepository.existsBySerialNumber(serialNumber);
    }
}
