package com.murungi.wilsonmurungimusalasoft.repository;

import com.murungi.wilsonmurungimusalasoft.model.Delivery;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface DeliveryRepository extends JpaRepository<Delivery, Long> {
    Optional<Delivery> findByDroneId(long droneId);

    Optional<Delivery> findByDroneIdEqualsAndStatusEquals(long droneId, String status);
}
