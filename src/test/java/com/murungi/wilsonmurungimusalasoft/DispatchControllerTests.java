package com.murungi.wilsonmurungimusalasoft;

import com.murungi.wilsonmurungimusalasoft.beans.DeliveryDao;
import com.murungi.wilsonmurungimusalasoft.beans.DeliveryMedicationDao;
import com.murungi.wilsonmurungimusalasoft.model.Delivery;
import com.murungi.wilsonmurungimusalasoft.model.DeliveryMedication;
import com.murungi.wilsonmurungimusalasoft.model.Drone;
import com.murungi.wilsonmurungimusalasoft.model.Medication;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class DispatchControllerTests extends WilsonMurungiMusalaSoftApplicationTests {

    //checking available drones for loading;
    @Test
    public void checkDronesAvailableForLoading_shouldReturnDronesInIdleStateAndBatteryAbove25() throws Exception {
        droneRepository.save(new Drone("SERIAL1", "Lightweight", 400.1, "IDLE", 100.0));
        droneRepository.save(new Drone("SERIAL2", "Lightweight", 400.1, "IDLE", 100.0));
        droneRepository.save(new Drone("SERIAL3", "Lightweight", 400.1, "IDLE", 24.0));
        droneRepository.save(new Drone("SERIAL4", "Lightweight", 400.1, "LOADING", 100.0));

        this.mockMvc.perform(
                get("/dispatch/availableDrones")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.payload", hasSize(2)))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));
    }

    //check drone battery level for a given drone;
    @Test
    public void checkBatteryLevel_shouldReturnSuccess() throws Exception {
        Drone drone = droneRepository.save(new Drone("SERIAL1", "Lightweight", 400.1, "IDLE", 100.0));
        this.mockMvc.perform(
                get("/drones/{id}", drone.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.payload.batteryLevel", is(drone.getBatteryLevel())))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));
    }

    //checking loaded medication items for a given drone;;
    @Test
    public void checkDroneLoad_shouldReturnSuccess() throws Exception {
        Drone drone = droneRepository.save(new Drone("SERIAL1", "Lightweight", 400.1, "DELIVERING", 100.0));
        Medication medication = medicationRepository.save(new Medication("Paracetamol", "PAR1038", 100.0, "https://dummy/path.jpg"));
        Delivery delivery = deliveryRepository.save(new Delivery(drone.getId(), "LOADING"));
        deliveryMedicationRepository.save(new DeliveryMedication(medication, 1, delivery));

        this.mockMvc.perform(
                get("/dispatch/droneLoad/{id}", drone.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.payload.load", hasSize(1)))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void batchLoadDroneWithCorrectLoadAndWithSufficientBatteryLevelAndIdleState_shouldReturnSuccess() throws Exception {
        Drone drone = droneRepository.save(new Drone("SERIAL1", "Lightweight", 400.1, "IDLE", 100.0));
        Medication medication = medicationRepository.save(new Medication("Paracetamol", "PAR1038", 100.0, "https://dummy/path.jpg"));

        DeliveryDao deliveryDao = new DeliveryDao();
        deliveryDao.setDroneId(drone.getId());
        List<DeliveryMedicationDao> medications = new ArrayList<>();
        medications.add(new DeliveryMedicationDao(medication.getId(), 1));
        deliveryDao.setMedications(medications);

        this.mockMvc.perform(
                post("/dispatch/load")
                        .content(asJsonString(deliveryDao))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("message", is("Delivery dispatched successfully")))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));

    }

    @Test
    public void batchLoadDroneWithCorrectLoadAndWithInsufficientBatteryLevelAndIdleState_shouldReturnFail() throws Exception {
        Drone drone = droneRepository.save(new Drone("SERIAL1", "Lightweight", 400.1, "IDLE", 10.0));
        Medication medication = medicationRepository.save(new Medication("Paracetamol", "PAR1038", 100.0, "https://dummy/path.jpg"));

        DeliveryDao deliveryDao = new DeliveryDao();
        deliveryDao.setDroneId(drone.getId());
        List<DeliveryMedicationDao> medications = new ArrayList<>();
        medications.add(new DeliveryMedicationDao(medication.getId(), 1));
        deliveryDao.setMedications(medications);

        this.mockMvc.perform(
                post("/dispatch/load")
                        .content(asJsonString(deliveryDao))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("message", is("Drone battery level below 25%")))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void batchLoadDroneWithMoreLoadAndWithSufficientBatteryLevelAndIdleState_shouldReturnFail() throws Exception {
        Drone drone = droneRepository.save(new Drone("SERIAL1", "Lightweight", 400.1, "IDLE", 100.0));
        Medication medication = medicationRepository.save(new Medication("Paracetamol", "PAR1038", 100.0, "https://dummy/path.jpg"));

        DeliveryDao deliveryDao = new DeliveryDao();
        deliveryDao.setDroneId(drone.getId());
        List<DeliveryMedicationDao> medications = new ArrayList<>();
        medications.add(new DeliveryMedicationDao(medication.getId(), 10));
        deliveryDao.setMedications(medications);

        this.mockMvc.perform(
                post("/dispatch/load")
                        .content(asJsonString(deliveryDao))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("message", is("Drone can not be overloaded")))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void batchLoadDroneWithCorrectLoadAndWithSufficientBatteryLevelAndNonIdleState_shouldReturnFail() throws Exception {
        Drone drone = droneRepository.save(new Drone("SERIAL1", "Lightweight", 400.1, "RETURNING", 100.0));
        Medication medication = medicationRepository.save(new Medication("Paracetamol", "PAR1038", 100.0, "https://dummy/path.jpg"));

        DeliveryDao deliveryDao = new DeliveryDao();
        deliveryDao.setDroneId(drone.getId());
        List<DeliveryMedicationDao> medications = new ArrayList<>();
        medications.add(new DeliveryMedicationDao(medication.getId(), 1));
        deliveryDao.setMedications(medications);

        this.mockMvc.perform(
                post("/dispatch/load")
                        .content(asJsonString(deliveryDao))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("message", is("Drone not in IDLE or LOADING state")))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void batchLoadDroneWithInvalidMedication_shouldReturnFail() throws Exception {
        Drone drone = droneRepository.save(new Drone("SERIAL1", "Lightweight", 400.1, "IDLE", 100.0));
        DeliveryDao deliveryDao = new DeliveryDao();
        deliveryDao.setDroneId(drone.getId());
        List<DeliveryMedicationDao> medications = new ArrayList<>();
        medications.add(new DeliveryMedicationDao(1, 1));
        deliveryDao.setMedications(medications);

        this.mockMvc.perform(
                post("/dispatch/load")
                        .content(asJsonString(deliveryDao))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("message", is("Invalid medication id 1")))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));
    }

}
