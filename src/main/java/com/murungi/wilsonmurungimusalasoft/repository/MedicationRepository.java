package com.murungi.wilsonmurungimusalasoft.repository;

import com.murungi.wilsonmurungimusalasoft.model.Medication;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface MedicationRepository extends JpaRepository<Medication, Long> {
    Optional<Medication> findMedicationByCode(String code);

    boolean existsByCode(String code);
}
