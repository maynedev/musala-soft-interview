package com.murungi.wilsonmurungimusalasoft.model.customValidation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = UniqueSerialNumberValidator.class)
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface UniqueSerialNumber {
    String message() default "Serial number already exists";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
