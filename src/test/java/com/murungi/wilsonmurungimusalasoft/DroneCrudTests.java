package com.murungi.wilsonmurungimusalasoft;


import com.murungi.wilsonmurungimusalasoft.model.Drone;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class DroneCrudTests extends WilsonMurungiMusalaSoftApplicationTests {

    //UNIT TESTS for creating drone
    //Happy path for creating drone
    @Test
    public void CreateDrone_ShouldReturnSuccess() throws Exception {
        String body = "{\"serialNumber\": \"SERIAL1\",\"model\": \"Lightweight\",\"state\": \"IDLE\",\"weight\": \"500.0\",\"batteryLevel\": \"100.0\"}";
        this.mockMvc.perform(
                post("/drones")
                        .content(body)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isCreated())
                .andExpect(jsonPath("message", is("Drone registered")))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));

        Assertions.assertTrue(droneRepository.existsBySerialNumber("SERIAL1"));
    }

    //Negative tests for creating drone
    @Test
    public void createWithoutSerialNumber_ShouldReturnBadRequest() throws Exception {
        String body = "{\"model\": \"Lightweight\",\"state\": \"IDLE\",\"weight\": \"500.0\",\"batteryLevel\": \"100.0\"}";
        this.mockMvc.perform(
                post("/drones")
                        .content(body)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("serialNumber", is("Serial number is mandatory")))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));

        Assertions.assertFalse(droneRepository.existsBySerialNumber("SERIAL1"));
    }

    @Test
    public void createWithDuplicateSerialNumber_ShouldReturnBadRequest() throws Exception {
        Drone drone = new Drone("SERIAL1", "Lightweight", 456.3, "LOADING", 100.0);
        droneRepository.save(drone);

        this.mockMvc.perform(
                post("/drones")
                        .content(asJsonString(drone))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("serialNumber", is("Serial number already exists")))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));

        Assertions.assertTrue(droneRepository.existsBySerialNumber("SERIAL1"));
    }

    @Test
    public void createWithLongSerialnumber_ShouldReturnBadRequest() throws Exception {
        String serial = RandomStringUtils.randomAlphanumeric(101);
        Drone drone = new Drone(serial, "Lightweight", 400.1, "LOADING", 100.0);
        this.mockMvc.perform(
                post("/drones")
                        .content(asJsonString(drone))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("serialNumber", is("size must be between 1 and 100")))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));

        Assertions.assertFalse(droneRepository.existsBySerialNumber(serial));
    }

    @Test
    public void createWithInvalidSerialnumber_ShouldReturnBadRequest() throws Exception {
        String serial = "*#SERIAL";
        Drone drone = new Drone(serial, "Lightweight", 400.1, "LOADING", 100.0);
        this.mockMvc.perform(
                post("/drones")
                        .content(asJsonString(drone))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("serialNumber", is("Invalid serial number")))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));

        Assertions.assertFalse(droneRepository.existsBySerialNumber(serial));
    }

    @Test
    public void createWithMoreWeightLimit_ShouldReturnBadRequest() throws Exception {
        weightNegativeTests(700, "must be less than or equal to 500");
    }

    @Test
    public void createWithLessWeightLimit_ShouldReturnBadRequest() throws Exception {
        weightNegativeTests(0, "must be greater than or equal to 1");
    }

    private void weightNegativeTests(double weight, String assertion) throws Exception {
        Drone drone = new Drone("SERIAL1", "Lightweight", weight, "LOADING", 100.0);
        this.mockMvc.perform(
                post("/drones")
                        .content(asJsonString(drone))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("weight", is(assertion)))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));

        Assertions.assertFalse(droneRepository.existsBySerialNumber("SERIAL1"));
    }


    @Test
    public void createWithAbove100batteryLevel_ShouldReturnBadRequest() throws Exception {
        batteryNegativeTests(101, "must be less than or equal to 100");
    }

    @Test
    public void createWithNegativeBattery_ShouldReturnBadRequest() throws Exception {
        batteryNegativeTests(-1, "must be greater than or equal to 0");
    }

    private void batteryNegativeTests(double batteryLevel, String assertion) throws Exception {
        Drone drone = new Drone("SERIAL1", "Lightweight", 200.0, "LOADING", batteryLevel);
        this.mockMvc.perform(
                post("/drones")
                        .content(asJsonString(drone))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("batteryLevel", is(assertion)))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));

        Assertions.assertFalse(droneRepository.existsBySerialNumber("SERIAL1"));
    }

    @Test
    public void createWithInvalidModel_ShouldReturnBadRequest() throws Exception {
        modelNegativeTest("INVALIDMODEL", "must be of any Lightweight, Middleweight, Cruiserweight or Heavyweight");
    }

    @Test
    public void createWithNullModel_ShouldReturnBadRequest() throws Exception {
        modelNegativeTest(null, "Model is mandatory");
    }

    public void modelNegativeTest(String model, String assertion) throws Exception {
        Drone drone = new Drone("SERIAL1", model, 400.1, "LOADING", 100.0);
        this.mockMvc.perform(
                post("/drones")
                        .content(asJsonString(drone))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("model", is(assertion)))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void createWithInvalidState_ShouldReturnBadRequest() throws Exception {
        stateNegativeTest("INVALIDSTATE", "must be of any IDLE, LOADING, LOADED, DELIVERING, DELIVERED or RETURNING");
    }

    @Test
    public void createWithNullState_ShouldReturnBadRequest() throws Exception {
        stateNegativeTest(null, "State is mandatory");
    }

    public void stateNegativeTest(String state, String assertion) throws Exception {
        Drone drone = new Drone("SERIAL1", "Lightweight", 400.1, state, 100.0);
        this.mockMvc.perform(
                post("/drones")
                        .content(asJsonString(drone))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("state", is(assertion)))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));
    }


    //UNIT TESTS for querying drone

    @Test
    public void getAllDrones_ShouldReturnAllDrones() throws Exception {
        droneRepository.save(new Drone("SERIAL1", "Lightweight", 400.1, "IDLE", 100.0));
        droneRepository.save(new Drone("SERIAL2", "Lightweight", 400.1, "IDLE", 100.0));

        this.mockMvc.perform(
                get("/drones")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.payload", hasSize(2)))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void getDroneByIdDrones_ShouldReturnSuccessIfDroneExists() throws Exception {
        Drone drone = droneRepository.save(new Drone("SERIAL1", "Lightweight", 400.1, "IDLE", 100.0));

        this.mockMvc.perform(
                get("/drones/{id}", drone.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.payload.serialNumber", is(drone.getSerialNumber())))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void getDroneByInvalidDroneId_ShouldReturnNotFound() throws Exception {
        this.mockMvc.perform(
                get("/drones/{id}", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("message", is("No drone registered with supplied ID")))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void deleteDroneByIdDrones_ShouldReturnSuccessIfDroneExists() throws Exception {
        Drone drone = droneRepository.save(new Drone("SERIAL1", "Lightweight", 400.1, "IDLE", 100.0));

        this.mockMvc.perform(
                delete("/drones/{id}", drone.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isNoContent())
                .andExpect(jsonPath("message", is("Drone deleted")))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void deleteDroneByInvalidDroneId_ShouldReturnNotFound() throws Exception {
        this.mockMvc.perform(
                delete("/drones/{id}", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("message", is("No drone registered with supplied ID")))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));
    }

    //UNIT TESTS for updating drone

    @Test
    public void updateDrone_ShouldReturnSuccess() throws Exception {
        Drone drone = droneRepository.save(new Drone("SERIAL1", "Lightweight", 400.1, "IDLE", 100.0));
        drone.setWeight(500.0);
        drone.setModel("Middleweight");
        drone.setState("LOADING");
        this.mockMvc.perform(
                put("/drones/{id}", drone.getId())
                        .content(asJsonString(drone))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("message", is("Drone details updated")))
                .andExpect(jsonPath("$.payload.model", is(drone.getModel())))
                .andExpect(jsonPath("$.payload.state", is(drone.getState())))
                .andExpect(jsonPath("$.payload.weight", is(drone.getWeight())))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));
    }


    @Test
    public void updateInvalidId_ShouldReturnNotFound() throws Exception {

        Drone drone = droneRepository.save(new Drone("SERIAL1", "Lightweight", 400.1, "IDLE", 100.0));

        this.mockMvc.perform(
                put("/drones/20", drone.getId())
                        .content(asJsonString(drone))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("message", is("Invalid drone ID passed")))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void updateWithoutSerialNumber_ShouldReturnBadRequest() throws Exception {

        Drone drone = droneRepository.save(new Drone("SERIAL1", "Lightweight", 400.1, "IDLE", 100.0));
        drone.setSerialNumber(null);

        this.mockMvc.perform(
                put("/drones/{id}", drone.getId())
                        .content(asJsonString(drone))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("serialNumber", is("Serial number is mandatory")))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void updateWithDuplicateSerialNumber_ShouldReturnBadRequest() throws Exception {
        Drone drone = new Drone("SERIAL1", "Lightweight", 456.3, "LOADING", 100.0);
        droneRepository.save(drone);

        Drone drone2 = droneRepository.save(new Drone("SERIAL2", "Lightweight", 400.1, "IDLE", 100.0));
        drone2.setSerialNumber(drone.getSerialNumber());

        this.mockMvc.perform(
                put("/drones/{id}", drone2.getId())
                        .content(asJsonString(drone))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("serialNumber", is("Serial number duplicate found")))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void updateWithLongSerialnumber_ShouldReturnBadRequest() throws Exception {
        String serial = RandomStringUtils.randomAlphanumeric(101); //Invalid serial number
        Drone drone = droneRepository.save(new Drone("SERIAL1", "Lightweight", 400.1, "IDLE", 100.0));
        drone.setSerialNumber(serial);

        this.mockMvc.perform(
                put("/drones/{id}", drone.getId())
                        .content(asJsonString(drone))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("serialNumber", is("size must be between 1 and 100")))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));

        Assertions.assertFalse(droneRepository.existsBySerialNumber(serial));
    }

    @Test
    public void updateWithInvalidSerialnumber_ShouldReturnBadRequest() throws Exception {
        String serial = "*#SERIAL";
        Drone drone = droneRepository.save(new Drone("SERIAL1", "Lightweight", 400.1, "IDLE", 100.0));
        drone.setSerialNumber(serial);

        this.mockMvc.perform(
                put("/drones/{id}", drone.getId())
                        .content(asJsonString(drone))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("serialNumber", is("Invalid serial number")))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));

        Assertions.assertFalse(droneRepository.existsBySerialNumber(serial));
    }

    @Test
    public void updateWithMoreWeightLimit_ShouldReturnBadRequest() throws Exception {
        updateWeightNegativeTests(700, "must be less than or equal to 500");
    }

    @Test
    public void updateWithLessWeightLimit_ShouldReturnBadRequest() throws Exception {
        updateWeightNegativeTests(0, "must be greater than or equal to 1");
    }

    private void updateWeightNegativeTests(double weight, String assertion) throws Exception {
        Drone drone = droneRepository.save(new Drone("SERIAL1", "Lightweight", 400.1, "IDLE", 100.0));
        drone.setWeight(weight);

        this.mockMvc.perform(
                put("/drones/{id}", drone.getId())
                        .content(asJsonString(drone))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("weight", is(assertion)))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));

    }


    @Test
    public void updateWithAbove100batteryLevel_ShouldReturnBadRequest() throws Exception {
        updateBatteryNegativeTests(101, "must be less than or equal to 100");
    }

    @Test
    public void updateWithNegativeBattery_ShouldReturnBadRequest() throws Exception {
        updateBatteryNegativeTests(-1, "must be greater than or equal to 0");
    }

    private void updateBatteryNegativeTests(double batteryLevel, String assertion) throws Exception {
        Drone drone = droneRepository.save(new Drone("SERIAL1", "Lightweight", 400.1, "IDLE", 100.0));
        drone.setBatteryLevel(batteryLevel);

        this.mockMvc.perform(
                put("/drones/{id}", drone.getId())
                        .content(asJsonString(drone))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("batteryLevel", is(assertion)))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));

    }

    @Test
    public void updateWithInvalidModel_ShouldReturnBadRequest() throws Exception {
        updateModelNegativeTest("INVALIDMODEL", "must be of any Lightweight, Middleweight, Cruiserweight or Heavyweight");
    }

    @Test
    public void updateWithNullModel_ShouldReturnBadRequest() throws Exception {
        updateModelNegativeTest(null, "Model is mandatory");
    }

    public void updateModelNegativeTest(String model, String assertion) throws Exception {
        Drone drone = droneRepository.save(new Drone("SERIAL1", "Lightweight", 400.1, "IDLE", 100.0));
        drone.setModel(model);

        this.mockMvc.perform(
                put("/drones/{id}", drone.getId())
                        .content(asJsonString(drone))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("model", is(assertion)))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void updateWithInvalidState_ShouldReturnBadRequest() throws Exception {
        updateStateNegativeTest("INVALIDSTATE", "must be of any IDLE, LOADING, LOADED, DELIVERING, DELIVERED or RETURNING");
    }

    @Test
    public void updateWithNullState_ShouldReturnBadRequest() throws Exception {
        updateStateNegativeTest(null, "State is mandatory");
    }

    public void updateStateNegativeTest(String state, String assertion) throws Exception {
        Drone drone = droneRepository.save(new Drone("SERIAL1", "Lightweight", 400.1, "IDLE", 100.0));
        drone.setState(state);

        this.mockMvc.perform(
                put("/drones/{id}", drone.getId())
                        .content(asJsonString(drone))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("state", is(assertion)))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));
    }


}
