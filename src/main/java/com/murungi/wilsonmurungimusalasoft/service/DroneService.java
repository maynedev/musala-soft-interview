package com.murungi.wilsonmurungimusalasoft.service;

import com.murungi.wilsonmurungimusalasoft.helper.HttpResponse;
import com.murungi.wilsonmurungimusalasoft.model.Delivery;
import com.murungi.wilsonmurungimusalasoft.model.Drone;
import com.murungi.wilsonmurungimusalasoft.repository.DeliveryRepository;
import com.murungi.wilsonmurungimusalasoft.repository.DroneRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class DroneService {
    public final DroneRepository droneRepository;
    public final DeliveryRepository deliveryRepository;
    private static final Logger LOGGER = LoggerFactory.getLogger(DroneService.class.getSimpleName());

    public DroneService(DroneRepository droneRepository, DeliveryRepository deliveryRepository) {
        this.droneRepository = droneRepository;
        this.deliveryRepository = deliveryRepository;
    }

    /**
     * Register new drones
     *
     * @param drone Drone details object
     * @return creation status or errors encountered
     */
    public ResponseEntity<?> create(Drone drone) {
        //Validate Serial number duplication
        if (droneRepository.findDroneBySerialNumber(drone.getSerialNumber()).isPresent())
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("{\"serialNumber\":\"Serial number already exists\"}");
        return new ResponseEntity<>(new HttpResponse("Drone registered", droneRepository.save(drone)), HttpStatus.CREATED);
    }

    /**
     * Update existing drones
     *
     * @param id         droneId
     * @param updateBody Drone details to update with
     * @return update status
     */
    public ResponseEntity<?> update(Long id, Drone updateBody) {

        //Check if drone with specified ID exists
        if (droneRepository.findById(id).isEmpty())
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body("{\"message\":\"Invalid drone ID passed\"}");

        //Validate Serial number duplication
        if (updateBody.getSerialNumber() != null) {
            Optional<Drone> optional = droneRepository.findDroneBySerialNumber(updateBody.getSerialNumber());
            if (optional.isPresent() && !optional.get().getId().equals(id))
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body("{\"serialNumber\":\"Serial number duplicate found\"}");
        }

        updateBody.setId(id);
        return new ResponseEntity<>(new HttpResponse("Drone details updated", droneRepository.save(updateBody)), HttpStatus.OK);
    }

    /**
     * @return All registered drones
     */
    public ResponseEntity<?> getAll() {
        return new ResponseEntity<>(new HttpResponse("Registered drones", droneRepository.findAll()), HttpStatus.OK);
    }

    /**
     * Get drone details by id
     *
     * @param id droneId
     * @return drone details or 404 if not available
     */
    public ResponseEntity<?> getById(long id) {
        Optional<Drone> optionalDrone = droneRepository.findById(id);
        if (optionalDrone.isEmpty())
            return new ResponseEntity<>(new HttpResponse("No drone registered with supplied ID", null), HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(new HttpResponse("Registered drone found", droneRepository.getById(id)), HttpStatus.OK);
    }

    /**
     * Delete a drone by ID
     *
     * @param id droneId
     * @return deletion status if available
     */
    public ResponseEntity<?> deleteById(long id) {
        Optional<Drone> optionalDrone = droneRepository.findById(id);
        if (optionalDrone.isEmpty())
            return new ResponseEntity<>(new HttpResponse("No drone registered with supplied ID", null), HttpStatus.NOT_FOUND);
        droneRepository.deleteById(id);
        return new ResponseEntity<>(new HttpResponse("Drone deleted", null), HttpStatus.NO_CONTENT);

    }

    /**
     * Periodic task to monitor drone battery and status
     * Assumptions if a drone's previous state is operational(DELIVERING, RETURNING). Reduce battery level by 10%.
     * 10% is is to avoid a drone going to 0% before returning. Lowest possible will be 5%
     * Each cycle represents is equivalent to a single journey if drone was operational.
     */
    @Scheduled(fixedDelay = 10000)
    public void monitorDronePower() {
        //Assume drone loses 1% every second it is in motion
        List<Drone> drones = droneRepository.findAll();
        Map<String, String> auditLog = new HashMap<>();
        for (Drone drone : drones) {
            auditLog.put("droneSerialNumber", drone.getSerialNumber());
            auditLog.put("state", drone.getState());
            auditLog.put("batteryLevel", String.valueOf(drone.getBatteryLevel()));
            LOGGER.info(auditLog.toString());

            //Put DRONE in the next state if operational
            if (drone.getState().equals("DELIVERING") || drone.getState().equals("RETURNING")) {
                drone.setBatteryLevel(drone.getBatteryLevel() - 10);
                drone.setState(drone.getState().equals("DELIVERING") ? "DELIVERED" : "IDLE");

                //if delivering mark the current delivery do DELIVERED
                Optional<Delivery> optionalDelivery = deliveryRepository.findByDroneId(drone.getId());
                if (optionalDelivery.isPresent() && drone.getState().equals("DELIVERED")) {
                    Delivery delivery = optionalDelivery.get();
                    delivery.setStatus("DELIVERED");
                    deliveryRepository.save(delivery);
                }
            } else if (drone.getState().equals("DELIVERED")) {
                drone.setState("RETURNING");
            }

            droneRepository.save(drone);
        }
    }
}
