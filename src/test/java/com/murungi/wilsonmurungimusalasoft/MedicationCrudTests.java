package com.murungi.wilsonmurungimusalasoft;


import com.murungi.wilsonmurungimusalasoft.model.Medication;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class MedicationCrudTests extends WilsonMurungiMusalaSoftApplicationTests {

    //UNIT TESTS for creating
    //Happy path for creating
    @Test
    public void registerMedication_ShouldReturnSuccess() throws Exception {
        String body = "{\"name\": \"Paracetamol\",\"code\": \"PAR1038\",\"image\": \"https://dummy/path.jpg\",\"weight\": \"500.0\"}";
        this.mockMvc.perform(
                post("/medications")
                        .content(body)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isCreated())
                .andExpect(jsonPath("message", is("Medication registered")))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));

        Assertions.assertTrue(medicationRepository.existsByCode("PAR1038"));
    }

    @Test
    public void registerWithLowercaseCode_ShouldReturnUppercaseCode() throws Exception {
        String body = "{\"name\": \"Paracetamol\",\"code\": \"para123\",\"image\": \"https://dummy/path.jpg\",\"weight\": \"500.0\"}";
        this.mockMvc.perform(
                post("/medications")
                        .content(body)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.payload.code", is("PARA123")))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));

        Assertions.assertTrue(medicationRepository.existsByCode("PARA123"));
    }

    //Negative tests for creating
    @Test
    public void createWithoutName_ShouldReturnBadRequest() throws Exception {
        String body = "{\"code\": \"PAR1038\",\"image\": \"https://dummy/path.jpg\",\"weight\": \"500.0\"}";
        this.mockMvc.perform(
                post("/medications")
                        .content(body)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("name", is("Name is mandatory")))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));

        Assertions.assertFalse(medicationRepository.existsByCode("PAR1038"));
    }

    @Test
    public void createWithInvalidName_ShouldReturnBadRequest() throws Exception {
        String body = "{\"name\": \"Paracetamol*$dsds\",\"code\": \"PAR1038\",\"image\": \"https://dummy/path.jpg\",\"weight\": \"500.0\"}";
        this.mockMvc.perform(
                post("/medications")
                        .content(body)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("name", is("Invalid medication name")))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));

        Assertions.assertFalse(medicationRepository.existsByCode("PAR1038"));
    }

    @Test
    public void createWithoutCode_ShouldReturnBadRequest() throws Exception {
        String body = "{\"name\": \"Paracetamol\",\"image\": \"https://dummy/path.jpg\",\"weight\": \"500.0\"}";
        this.mockMvc.perform(
                post("/medications")
                        .content(body)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("code", is("Code is mandatory")))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));

        Assertions.assertFalse(medicationRepository.existsByCode("PAR1038"));
    }

    @Test
    public void createWithInvalidCode_ShouldReturnBadRequest() throws Exception {
        String body = "{\"name\": \"Paracetamol\",\"code\": \"PAR1038*#\",\"image\": \"https://dummy/path.jpg\",\"weight\": \"500.0\"}";
        this.mockMvc.perform(
                post("/medications")
                        .content(body)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("code", is("Invalid medication code")))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));

        Assertions.assertFalse(medicationRepository.existsByCode("PAR1038"));
    }

    @Test
    public void createWithDuplicateCode_ShouldReturnBadRequest() throws Exception {
        medicationRepository.save(new Medication("Paracetamol", "PAR1038", 456.3, "https://dummy/path.jpg"));

        String body = "{\"name\": \"Paracetamol\",\"code\": \"PAR1038\",\"image\": \"https://dummy/path.jpg\",\"weight\": \"500.0\"}";
        this.mockMvc.perform(
                post("/medications")
                        .content(body)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("code", is("Medication code already exists")))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void createWithoutWeight_ShouldReturnBadRequest() throws Exception {
        String body = "{\"name\": \"Paracetamol\",\"code\": \"PAR1038\",\"image\": \"https://dummy/path.jpg\"}";
        this.mockMvc.perform(
                post("/medications")
                        .content(body)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("weight", is("Weight is mandatory")))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void createWithMoreWeightLimit_ShouldReturnBadRequest() throws Exception {
        weightNegativeTests(700, "must be less than or equal to 500");
    }

    @Test
    public void createWithLessWeightLimit_ShouldReturnBadRequest() throws Exception {
        weightNegativeTests(0, "must be greater than or equal to 1");
    }

    private void weightNegativeTests(double weight, String assertion) throws Exception {
        Medication medication = new Medication("Paracetamol", "PAR1038", weight, "https://dummy/path.jpg");
        this.mockMvc.perform(
                post("/medications")
                        .content(asJsonString(medication))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("weight", is(assertion)))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));

        Assertions.assertFalse(medicationRepository.existsByCode("PAR1038"));
    }

    @Test
    public void createWithoutImage_ShouldReturnBadRequest() throws Exception {
        String body = "{\"name\": \"Paracetamol\",\"code\": \"PAR1038\",\"weight\": \"400.1\"}";
        this.mockMvc.perform(
                post("/medications")
                        .content(body)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("image", is("Image is mandatory")))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void createWithInvalidImageAddress_ShouldReturnBadRequest() throws Exception {
        String body = "{\"name\": \"Paracetamol\",\"code\": \"PAR1038\",\"weight\": \"400.1\",\"image\": \"https://dummy/painvalid_path\"}";
        this.mockMvc.perform(
                post("/medications")
                        .content(body)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("image", is("Invalid image path")))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));
    }

    //Happy path for updating
    @Test
    public void updateMedication_ShouldReturnSuccess() throws Exception {
        Medication medication = medicationRepository.save(new Medication("Paracetamol", "PAR1038", 100.0, "https://dummy/path.jpg"));
        String body = "{\"name\": \"Paracetamol\",\"code\": \"PAR103\",\"image\": \"https://dummy/path2.jpg\",\"weight\": \"500.0\"}";
        this.mockMvc.perform(
                put("/medications/{id}", medication.getId())
                        .content(body)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("message", is("Medication details updated")))
                .andExpect(jsonPath("$.payload.weight", is(500.0)))
                .andExpect(jsonPath("$.payload.image", is("https://dummy/path2.jpg")))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));

        Assertions.assertTrue(medicationRepository.existsByCode("PAR103"));
    }

    @Test
    public void updateWithLowercaseCode_ShouldReturnUppercaseCode() throws Exception {
        Medication medication = medicationRepository.save(new Medication("Paracetamol", "PAR1038", 100.0, "https://dummy/path.jpg"));
        String body = "{\"name\": \"Paracetamol\",\"code\": \"para123\",\"image\": \"https://dummy/path.jpg\",\"weight\": \"500.0\"}";
        this.mockMvc.perform(
                put("/medications/{id}", medication.getId())
                        .content(body)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.payload.code", is("PARA123")))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));

        Assertions.assertTrue(medicationRepository.existsByCode("PARA123"));
    }

    //Negative tests for updating
    @Test
    public void updateWithoutName_ShouldReturnBadRequest() throws Exception {
        Medication medication = medicationRepository.save(new Medication("Paracetamol", "PAR1038", 100.0, "https://dummy/path.jpg"));
        String body = "{\"code\": \"PAR1038\",\"image\": \"https://dummy/path.jpg\",\"weight\": \"500.0\"}";
        this.mockMvc.perform(
                put("/medications/{id}", medication.getId())
                        .content(body)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("name", is("Name is mandatory")))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void updateWithInvalidId_ShouldReturnNotFound() throws Exception {
        String body = "{\"name\": \"Paracetamol\",\"code\": \"PAR1038\",\"image\": \"https://dummy/path.jpg\",\"weight\": \"500.0\"}";
        this.mockMvc.perform(
                put("/medications/{id}", 1)
                        .content(body)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("message", is("Invalid medication ID passed")))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));

        Assertions.assertFalse(medicationRepository.existsByCode("PAR1038"));
    }

    @Test
    public void updateWithInvalidName_ShouldReturnBadRequest() throws Exception {
        Medication medication = medicationRepository.save(new Medication("Paracetamol", "PAR1038", 100.0, "https://dummy/path.jpg"));
        String body = "{\"name\": \"Paracetamol*$dsds\",\"code\": \"PAR1038\",\"image\": \"https://dummy/path.jpg\",\"weight\": \"500.0\"}";
        this.mockMvc.perform(
                put("/medications/{id}", medication.getId())
                        .content(body)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("name", is("Invalid medication name")))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));

    }

    @Test
    public void updateWithoutCode_ShouldReturnBadRequest() throws Exception {
        Medication medication = medicationRepository.save(new Medication("Paracetamol", "PAR1038", 100.0, "https://dummy/path.jpg"));
        String body = "{\"name\": \"Paracetamol\",\"image\": \"https://dummy/path.jpg\",\"weight\": \"500.0\"}";
        this.mockMvc.perform(
                put("/medications/{id}", medication.getId())
                        .content(body)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("code", is("Code is mandatory")))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void updateWithInvalidCode_ShouldReturnBadRequest() throws Exception {
        Medication medication = medicationRepository.save(new Medication("Paracetamol", "PAR1038", 100.0, "https://dummy/path.jpg"));
        String body = "{\"name\": \"Paracetamol\",\"code\": \"PAR1038*#\",\"image\": \"https://dummy/path.jpg\",\"weight\": \"500.0\"}";
        this.mockMvc.perform(
                put("/medications/{id}", medication.getId())
                        .content(body)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("code", is("Invalid medication code")))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));

    }

    @Test
    public void updateWithDuplicateCode_ShouldReturnBadRequest() throws Exception {
        medicationRepository.save(new Medication("Paracetamol", "PAR1038", 100.0, "https://dummy/path.jpg"));
        Medication medication = medicationRepository.save(new Medication("Paracetamol", "PAR1039", 100.0, "https://dummy/path.jpg"));

        String body = "{\"name\": \"Paracetamol\",\"code\": \"PAR1038\",\"image\": \"https://dummy/path.jpg\",\"weight\": \"500.0\"}";
        this.mockMvc.perform(
                put("/medications/{id}", medication.getId())
                        .content(body)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("code", is("Medication code already exists")))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void updateWithoutWeight_ShouldReturnBadRequest() throws Exception {
        Medication medication = medicationRepository.save(new Medication("Paracetamol", "PAR1038", 100.0, "https://dummy/path.jpg"));
        String body = "{\"name\": \"Paracetamol\",\"code\": \"PAR1038\",\"image\": \"https://dummy/path.jpg\"}";
        this.mockMvc.perform(
                put("/medications/{id}", medication.getId())
                        .content(body)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("weight", is("Weight is mandatory")))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void updateWithMoreWeightLimit_ShouldReturnBadRequest() throws Exception {
        updateWeightNegativeTests(700, "must be less than or equal to 500");
    }

    @Test
    public void updateWithLessWeightLimit_ShouldReturnBadRequest() throws Exception {
        updateWeightNegativeTests(0, "must be greater than or equal to 1");
    }

    private void updateWeightNegativeTests(double weight, String assertion) throws Exception {
        Medication medication = medicationRepository.save(new Medication("Paracetamol", "PAR1038", 100.0, "https://dummy/path.jpg"));
        Medication body = new Medication("Paracetamol", "PAR1038", weight, "https://dummy/path.jpg");
        this.mockMvc.perform(
                put("/medications/{id}", medication.getId())
                        .content(asJsonString(body))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("weight", is(assertion)))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));

    }

    @Test
    public void updateWithoutImage_ShouldReturnBadRequest() throws Exception {
        Medication medication = medicationRepository.save(new Medication("Paracetamol", "PAR1038", 100.0, "https://dummy/path.jpg"));
        String body = "{\"name\": \"Paracetamol\",\"code\": \"PAR1038\",\"weight\": \"400.1\"}";
        this.mockMvc.perform(
                put("/medications/{id}", medication.getId())
                        .content(body)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("image", is("Image is mandatory")))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void updateWithInvalidImageAddress_ShouldReturnBadRequest() throws Exception {
        Medication medication = medicationRepository.save(new Medication("Paracetamol", "PAR1038", 100.0, "https://dummy/path.jpg"));
        String body = "{\"name\": \"Paracetamol\",\"code\": \"PAR1038\",\"weight\": \"400.1\",\"image\": \"https://dummy/painvalid_path\"}";
        this.mockMvc.perform(
                put("/medications/{id}", medication.getId())
                        .content(body)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("image", is("Invalid image path")))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));
    }


    //UNIT TESTS for querying
    @Test
    public void getAllMedication_ShouldReturnAll() throws Exception {
        medicationRepository.save(new Medication("Paracetamol", "PAR1038", 456.3, "https://dummy/path.jpg"));
        medicationRepository.save(new Medication("Paracetamol", "PAR1039", 456.3, "https://dummy/path.jpg"));

        this.mockMvc.perform(
                get("/medications")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.payload", hasSize(2)))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void getMedicationById_ShouldReturnSuccessIfMedicationExists() throws Exception {
        Medication medication = medicationRepository.save(new Medication("Paracetamol", "PAR1038", 456.3, "https://dummy/path.jpg"));

        this.mockMvc.perform(
                get("/medications/{id}", medication.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.payload.code", is(medication.getCode())))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void getMedicationByInvalidId_ShouldReturnNotFound() throws Exception {
        this.mockMvc.perform(
                get("/medications/{id}", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("message", is("No medication registered with supplied ID")))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void deleteMedicationById_ShouldReturnSuccessIfExists() throws Exception {

        Medication medication = medicationRepository.save(new Medication("Paracetamol", "PAR1038", 456.3, "https://dummy/path.jpg"));

        this.mockMvc.perform(
                delete("/medications/{id}", medication.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isNoContent())
                .andExpect(jsonPath("message", is("Medication deleted")))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void deleteMedicationByInvalidId_ShouldReturnNotFound() throws Exception {
        this.mockMvc.perform(
                delete("/medications/{id}", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("message", is("No medication registered with supplied ID")))
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON));
    }

}
