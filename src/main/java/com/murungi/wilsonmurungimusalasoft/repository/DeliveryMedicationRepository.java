package com.murungi.wilsonmurungimusalasoft.repository;

import com.murungi.wilsonmurungimusalasoft.model.DeliveryMedication;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DeliveryMedicationRepository extends JpaRepository<DeliveryMedication, Long> {
    List<DeliveryMedication> findAllByDeliveryId(long deliveryId);
}
