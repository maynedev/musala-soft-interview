FROM openjdk:14-alpine

VOLUME /tmp
COPY target/*.jar app.jar
RUN mkdir /var/log/murungiWilsonMusala
RUN chmod 777 -R /var/log/murungiWilsonMusala
ENTRYPOINT ["java","-jar","/app.jar"]