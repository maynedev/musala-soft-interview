package com.murungi.wilsonmurungimusalasoft.http;

import com.murungi.wilsonmurungimusalasoft.beans.DeliveryDao;
import com.murungi.wilsonmurungimusalasoft.beans.UpdateDeliveryDao;
import com.murungi.wilsonmurungimusalasoft.helper.HttpResponse;
import com.murungi.wilsonmurungimusalasoft.repository.DroneRepository;
import com.murungi.wilsonmurungimusalasoft.service.DispatchService;
import com.murungi.wilsonmurungimusalasoft.service.DroneService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController()
public class DispatchController extends Controller {

    final DroneService droneService;
    final DroneRepository droneRepository;
    final DispatchService dispatchService;

    public DispatchController(DroneService droneService, DroneRepository droneRepository, DispatchService dispatchService) {
        this.droneService = droneService;
        this.droneRepository = droneRepository;
        this.dispatchService = dispatchService;
    }

    @PutMapping(path = "/dispatch/update/{id}")
    public ResponseEntity<?> updateDeliveryStatus(@PathVariable long id, @RequestBody @Valid UpdateDeliveryDao updateDeliveryDao) {
        return dispatchService.updateDeliveryStatus(id, updateDeliveryDao);
    }

    @GetMapping(path = "/dispatch/droneLoad/{id}")
    public ResponseEntity<?> droneLoad(@PathVariable long id) {
        return dispatchService.getDroneLoad(id);
    }

    @GetMapping(path = "/dispatch/availableDrones")
    public ResponseEntity<HttpResponse> availableDrones() {
        return new ResponseEntity<>(new HttpResponse("Available drones for delivery", droneRepository.findAllByBatteryLevelGreaterThanEqualAndStateEquals(25.0, "IDLE")), HttpStatus.OK);
    }

    @PostMapping(path = "/dispatch/load")
    public ResponseEntity<HttpResponse> startLoading(@Valid @RequestBody DeliveryDao deliveryDao) {
        return dispatchService.load(deliveryDao);
    }

}
