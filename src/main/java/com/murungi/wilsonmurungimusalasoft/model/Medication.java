package com.murungi.wilsonmurungimusalasoft.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
public @Data
class Medication {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "Code is mandatory")
    @Size(min = 1, max = 100)
    @Column(length = 100, unique = true)
    @Pattern(regexp = "[A-Za-z0-9]+", message = "Invalid medication code")
    private String code;

    @NotNull(message = "Image is mandatory")
    @Pattern(regexp = "([^\\s]+(\\.(?i)(jpg|png|gif|bmp))$)", message = "Invalid image path")
    private String image;

    @NotNull(message = "Name is mandatory")
    @Pattern(regexp = "[A-Za-z0-9]+", message = "Invalid medication name")
    private String name;

    @NotNull(message = "Weight is mandatory")
    @Min(1)
    @Max(500)
    private Double weight;

    public Medication(String name, String code, Double weight, String image) {
        this.code = code;
        this.image = image;
        this.weight = weight;
        this.name = name;
    }

    public Medication() {
    }

}
