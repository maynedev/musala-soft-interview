package com.murungi.wilsonmurungimusalasoft.helper;

public class Constants {
    //Lightweight, Middleweight, Cruiserweight, Heavyweight
    public static String LIGHT_WEIGHT = "Lightweight";
    public static String MIDDLE_WEIGHT = "Middleweight";
    public static String CRUISE_WEIGHT = "Cruiserweight";
    public static String HEAVY_WEIGHT = "Heavyweight";

    public static String[] MODELS = new String[]{"Lightweight", "Middleweight", "Cruiserweight", "Heavyweight"};
    public static String[] DRONE_STATES = new String[]{"Lightweight", "Middleweight", "Cruiserweight", "Heavyweight"};


}
