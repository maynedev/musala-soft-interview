package com.murungi.wilsonmurungimusalasoft.beans;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class DeliveryMedicationDao {
    private long id;
    @NotNull
    private int units;
    @NotNull
    private long medicationId;

    public DeliveryMedicationDao(long medicationId, int units) {
        this.units = units;
        this.medicationId = medicationId;
    }
}
