package com.murungi.wilsonmurungimusalasoft.http;

import com.murungi.wilsonmurungimusalasoft.model.Medication;
import com.murungi.wilsonmurungimusalasoft.service.MedicationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController()
public class MedicationController extends Controller {

    private final MedicationService medicationService;

    public MedicationController(MedicationService medicationService) {
        this.medicationService = medicationService;
    }

    @GetMapping(path = "medications")
    public ResponseEntity<?> index() {
        return medicationService.getAll();
    }

    @PostMapping(path = "medications")
    public ResponseEntity<?> register(@Valid @RequestBody Medication medication) {
        return this.medicationService.create(medication);
    }

    @GetMapping(path = "medications/{id}")
    public ResponseEntity<?> show(@PathVariable("id") long id) {
        return medicationService.getById(id);
    }

    @PutMapping(path = "medications/{id}")
    public ResponseEntity<?> update(@PathVariable("id") long id, @Valid @RequestBody Medication medication) {
        return medicationService.update(id, medication);
    }

    @DeleteMapping(path = "medications/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") long id) {
        return medicationService.deleteById(id);
    }

}
