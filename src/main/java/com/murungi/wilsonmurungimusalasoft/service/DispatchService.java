package com.murungi.wilsonmurungimusalasoft.service;

import com.murungi.wilsonmurungimusalasoft.beans.DeliveryDao;
import com.murungi.wilsonmurungimusalasoft.beans.DeliveryMedicationDao;
import com.murungi.wilsonmurungimusalasoft.beans.UpdateDeliveryDao;
import com.murungi.wilsonmurungimusalasoft.helper.HttpResponse;
import com.murungi.wilsonmurungimusalasoft.model.*;
import com.murungi.wilsonmurungimusalasoft.repository.DeliveryMedicationRepository;
import com.murungi.wilsonmurungimusalasoft.repository.DeliveryRepository;
import com.murungi.wilsonmurungimusalasoft.repository.DroneRepository;
import com.murungi.wilsonmurungimusalasoft.repository.MedicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class DispatchService {

    final DroneRepository droneRepository;
    final MedicationRepository medicationRepository;
    final DeliveryRepository deliveryRepository;
    final DeliveryMedicationRepository deliveryMedicationRepository;

    public DispatchService(DroneRepository droneRepository, MedicationRepository medicationRepository, DeliveryRepository deliveryRepository, DeliveryMedicationRepository deliveryMedicationRepository) {
        this.droneRepository = droneRepository;
        this.medicationRepository = medicationRepository;
        this.deliveryRepository = deliveryRepository;
        this.deliveryMedicationRepository = deliveryMedicationRepository;
    }


    /**
     * Return current load on drone
     *
     * @param id droneId
     * @return Current delivery and medications on-board
     */
    public ResponseEntity<HttpResponse> getDroneLoad(long id) {
        Optional<Drone> optionalDrone = droneRepository.findById(id);
        if (optionalDrone.isEmpty()) {
            //No drone with specified ID found
            return new ResponseEntity<>(new HttpResponse("Invalid drone id passed", null), HttpStatus.NOT_FOUND);
        }

        Map<String, Object> payload = new HashMap<>();
        payload.put("drone", optionalDrone.get());
        Optional<Delivery> optionalDelivery = deliveryRepository.findByDroneIdEqualsAndStatusEquals(optionalDrone.get().getId(), "LOADING");
        optionalDelivery.ifPresent(delivery -> payload.put("load", deliveryMedicationRepository.findAllByDeliveryId(delivery.getId())));
        return new ResponseEntity<>(new HttpResponse("Drone loaded", payload), HttpStatus.OK);
    }


    /**
     * Batch load a drone
     *
     * @param deliveryDao Delivery object
     * @return Delivery schedule status or errors
     */
    public ResponseEntity<HttpResponse> load(DeliveryDao deliveryDao) {
        //Initialize reponse payload
        Map<String, Object> payload = new HashMap<>();
        Optional<Drone> optionalDrone = droneRepository.findById(deliveryDao.getDroneId());
        if (optionalDrone.isEmpty()) {
            //No drone with specified ID found
            payload.put("drone", null);
            return new ResponseEntity<>(new HttpResponse("Invalid drone id passed", payload), HttpStatus.BAD_REQUEST);
        }

        List<DeliveryMedication> deliveryMedications = new ArrayList<>();
        Drone drone = optionalDrone.get();

        payload.put("drone", drone);
        if (!drone.getState().equals("IDLE") && !drone.getState().equals("LOADING"))
            return new ResponseEntity<>(new HttpResponse("Drone not in IDLE or LOADING state", payload), HttpStatus.BAD_REQUEST);
        if (drone.getBatteryLevel() < 25)
            //Drone needs to be charged
            return new ResponseEntity<>(new HttpResponse("Drone battery level below 25%", payload), HttpStatus.BAD_REQUEST);

        //Ongoing delivery
        Optional<Delivery> optionalDelivery = deliveryRepository.findByDroneIdEqualsAndStatusEquals(optionalDrone.get().getId(), "LOADED");

        Delivery delivery = new Delivery();
        double medicationLoad = 0;
        //Calculate the current load on the drone
        if (optionalDelivery.isPresent()) {
            //Update existing delivery
            delivery = optionalDelivery.get();
            //Get current load on the drone
            List<DeliveryMedication> loadedMedication = deliveryMedicationRepository.findAllByDeliveryId(optionalDelivery.get().getId());
            for (DeliveryMedication medication : loadedMedication) {
                medicationLoad += medication.getMedication().getWeight() * medication.getUnits();
            }
        }

        for (DeliveryMedicationDao medicationDao : deliveryDao.getMedications()) {
            Optional<Medication> optionalMedication = medicationRepository.findById(medicationDao.getMedicationId());
            if (optionalMedication.isEmpty()) {
                //Return error. Medication not found
                return new ResponseEntity<>(new HttpResponse("Invalid medication id " + medicationDao.getMedicationId(), payload), HttpStatus.BAD_REQUEST);
            } else {
                Medication medication = optionalMedication.get();
                medicationLoad += medication.getWeight() * medicationDao.getUnits();
                deliveryMedications.add(new DeliveryMedication(optionalMedication.get(), medicationDao.getUnits(), null));
            }
        }

        if (medicationLoad > drone.getWeight()) {
            //Return error. Drone can not carry more than its capacity;
            return new ResponseEntity<>(new HttpResponse("Drone can not be overloaded", payload), HttpStatus.BAD_REQUEST);
        }

        delivery.setDroneId(drone.getId());
        delivery.setStatus("LOADED");
        Delivery delivery1 = deliveryRepository.save(delivery);

        for (DeliveryMedication medication : deliveryMedications) {
            medication.setDelivery(delivery1);
            deliveryMedicationRepository.save(medication);
        }

        //Set drone to delivering status
        drone.setState("LOADING");
        droneRepository.save(drone);

        payload.put("drone", drone);
        payload.put("delivery", delivery1);
        payload.put("load", deliveryMedicationRepository.findAllByDeliveryId(delivery1.getId()));
        return new ResponseEntity<>(new HttpResponse("Delivery dispatched successfully", payload), HttpStatus.OK);
    }


    /**
     * Manual process to update delivery states
     *
     * @param id                Delivery ID
     * @param updateDeliveryDao DAO containing update status
     * @return Update response or errors encountered
     */
    public ResponseEntity<?> updateDeliveryStatus(long id, UpdateDeliveryDao updateDeliveryDao) {
        Optional<Delivery> optionalDelivery = deliveryRepository.findById(id);
        if (optionalDelivery.isEmpty()) {
            //No delivery with specified ID found
            return new ResponseEntity<>(new HttpResponse("Invalid delivery id passed", null), HttpStatus.BAD_REQUEST);
        }
        Delivery delivery = optionalDelivery.get();

        Drone drone = droneRepository.getById(delivery.getDroneId());
        //Possible state LOADING, DISPATCHED, DELIVERED
        //Drone state IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING
        if (updateDeliveryDao.getStatus().equals("DISPATCHED")) {
            //update drone to DELIVERING
            drone.setState("DELIVERING");
        } else if (updateDeliveryDao.getStatus().equals("DELIVERED")) {
            //update drone to DELIVERED
            drone.setState("DELIVERED");
        }
        //Update drone status
        droneRepository.save(drone);

        return new ResponseEntity<>(new HttpResponse("Delivery state updated", delivery), HttpStatus.OK);
    }
}
