package com.murungi.wilsonmurungimusalasoft.service;

import com.murungi.wilsonmurungimusalasoft.helper.HttpResponse;
import com.murungi.wilsonmurungimusalasoft.model.Medication;
import com.murungi.wilsonmurungimusalasoft.repository.MedicationRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class MedicationService {
    public final MedicationRepository medicationRepository;

    public MedicationService(MedicationRepository MedicationRepository) {
        this.medicationRepository = MedicationRepository;
    }

    public ResponseEntity<?> create(Medication medication) {
        //Validate Serial number duplication
        if (medicationRepository.findMedicationByCode(medication.getCode()).isPresent())
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("{\"code\":\"Medication code already exists\"}");
        medication.setCode(medication.getCode().toUpperCase());
        return new ResponseEntity<>(new HttpResponse("Medication registered", medicationRepository.save(medication)), HttpStatus.CREATED);
    }

    public ResponseEntity<?> update(Long id, Medication updateBody) {

        //Check if Medication with specified ID exists
        if (medicationRepository.findById(id).isEmpty())
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body("{\"message\":\"Invalid medication ID passed\"}");

        //Validate Serial number duplication
        if (updateBody.getCode() != null) {
            Optional<Medication> optional = medicationRepository.findMedicationByCode(updateBody.getCode());
            if (optional.isPresent() && !optional.get().getId().equals(id))
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body("{\"code\":\"Medication code already exists\"}");
        }

        assert updateBody.getCode() != null;
        updateBody.setCode(updateBody.getCode().toUpperCase());
        updateBody.setId(id);
        return new ResponseEntity<>(new HttpResponse("Medication details updated", medicationRepository.save(updateBody)), HttpStatus.OK);
    }

    public ResponseEntity<?> getAll() {
        return new ResponseEntity<>(new HttpResponse("Registered Medications", medicationRepository.findAll()), HttpStatus.OK);
    }

    public ResponseEntity<?> getById(long id) {
        Optional<Medication> optionalMedication = medicationRepository.findById(id);
        if (optionalMedication.isEmpty())
            return new ResponseEntity<>(new HttpResponse("No medication registered with supplied ID", null), HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(new HttpResponse("Registered Medication found", medicationRepository.getById(id)), HttpStatus.OK);
    }

    public ResponseEntity<?> deleteById(long id) {
        Optional<Medication> optionalMedication = medicationRepository.findById(id);
        if (optionalMedication.isEmpty())
            return new ResponseEntity<>(new HttpResponse("No medication registered with supplied ID", null), HttpStatus.NOT_FOUND);
        medicationRepository.deleteById(id);
        return new ResponseEntity<>(new HttpResponse("Medication deleted", null), HttpStatus.NO_CONTENT);

    }
}
