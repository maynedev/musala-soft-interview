package com.murungi.wilsonmurungimusalasoft.http;

import com.murungi.wilsonmurungimusalasoft.model.Drone;
import com.murungi.wilsonmurungimusalasoft.service.DroneService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class DronesController extends Controller {

    private final DroneService droneService;

    public DronesController(DroneService droneService) {
        this.droneService = droneService;
    }

    @GetMapping(path = "drones")
    public ResponseEntity<?> index() {
        return droneService.getAll();
    }

    @PostMapping(path = "drones")
    public ResponseEntity<?> create(@Valid @RequestBody Drone drone) {
        return this.droneService.create(drone);
    }

    @GetMapping(path = "drones/{id}")
    public ResponseEntity<?> show(@PathVariable("id") long id) {
        return droneService.getById(id);
    }

    @PutMapping(path = "drones/{id}")
    public ResponseEntity<?> update(@PathVariable("id") long id, @Valid @RequestBody Drone drone) {
        return droneService.update(id, drone);
    }

    @DeleteMapping(path = "drones/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") long id) {
        return droneService.deleteById(id);
    }

}
