package com.murungi.wilsonmurungimusalasoft.repository;

import com.murungi.wilsonmurungimusalasoft.model.Drone;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface DroneRepository extends JpaRepository<Drone, Long> {
    boolean existsBySerialNumber(String serialNumber);

    Optional<Drone> findDroneBySerialNumber(String serialNumber);

    List<Drone> findAllByBatteryLevelGreaterThanEqualAndStateEquals(double batteryLevel, String state);
}
