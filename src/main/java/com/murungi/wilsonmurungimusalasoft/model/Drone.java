package com.murungi.wilsonmurungimusalasoft.model;

import com.murungi.wilsonmurungimusalasoft.model.customValidation.UniqueSerialNumber;
import com.murungi.wilsonmurungimusalasoft.model.customValidation.AnyOf;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
public @Data
class Drone {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "Serial number is mandatory")
    @Size(min = 1, max = 100)
    @Column(length = 100, unique = true)
//    @UniqueSerialNumber
    @Pattern(regexp = "[A-Za-z0-9]+", message = "Invalid serial number")
    private String serialNumber;

    @NotNull(message = "Model is mandatory")
    @AnyOf(enumClass = DroneModel.class, message = "must be of any Lightweight, Middleweight, Cruiserweight or Heavyweight")
    private String model;

    @Min(1)
    @Max(500)
    @NotNull(message = "Weight is mandatory")
    private Double weight;

    @NotNull(message = "State is mandatory")
    @AnyOf(enumClass = DroneState.class, message = "must be of any IDLE, LOADING, LOADED, DELIVERING, DELIVERED or RETURNING")
    private String state;

    @Min(0)
    @Max(100)
    @NotNull(message = "Battery level is mandatory")
    private Double batteryLevel;

    public Drone(String serialNumber, String model, Double weight, String state, Double batteryLevel) {
        this.serialNumber = serialNumber;
        this.model = model;
        this.weight = weight;
        this.state = state;
        this.batteryLevel = batteryLevel;
    }

    public Drone() {
    }

}

enum DroneModel {
    Lightweight, Middleweight, Cruiserweight, Heavyweight
}

enum DroneState {
    IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING
}