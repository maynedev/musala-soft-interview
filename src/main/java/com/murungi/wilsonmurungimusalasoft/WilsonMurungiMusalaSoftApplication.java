package com.murungi.wilsonmurungimusalasoft;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class WilsonMurungiMusalaSoftApplication {

    public static void main(String[] args) {
        SpringApplication.run(WilsonMurungiMusalaSoftApplication.class, args);
    }

}
