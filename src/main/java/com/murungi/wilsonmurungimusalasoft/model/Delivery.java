package com.murungi.wilsonmurungimusalasoft.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public @Data
class Delivery {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private long droneId;
    private String status;

    public Delivery(long droneId, String status) {
        this.droneId = droneId;
        this.status = status;
    }

    public Delivery() {
    }
}
