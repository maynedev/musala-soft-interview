package com.murungi.wilsonmurungimusalasoft;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.murungi.wilsonmurungimusalasoft.repository.DeliveryMedicationRepository;
import com.murungi.wilsonmurungimusalasoft.repository.DeliveryRepository;
import com.murungi.wilsonmurungimusalasoft.repository.DroneRepository;
import com.murungi.wilsonmurungimusalasoft.repository.MedicationRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
class WilsonMurungiMusalaSoftApplicationTests {


    @Autowired
    public MockMvc mockMvc;
    @Autowired
    public DroneRepository droneRepository;
    @Autowired
    public MedicationRepository medicationRepository;
    @Autowired
    public DeliveryRepository deliveryRepository;
    @Autowired
    public DeliveryMedicationRepository deliveryMedicationRepository;

    @Test
    void contextLoads() {
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @AfterEach
    @BeforeEach
    public void tear() {
        deliveryMedicationRepository.deleteAll();
        deliveryRepository.deleteAll();
        medicationRepository.deleteAll();
        droneRepository.deleteAll();
    }

}
